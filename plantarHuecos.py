# Plantar huecos a una serie de tiempo
import Helper
import redNeuronal
import neurolab as nl
import numpy as np
import math
import copy
import csv

directorio = 'datosExperimentosMuestras/'

numeroMuestra =  3000

datosArchivos = np.array([
                        # ['wind_aristeomercado_10m_muestra']
                        # ['wind_cointzio_10m_muestra']
                        ['wind_corrales_10m_muestra']
                        # ['wind_elfresno_10m_muestra']
                    ])

nb_archivo = datosArchivos[0][0]
rutaArchivo = directorio + nb_archivo+'.csv'

archivo = Helper.funcionLeerArchivoParaImplantarHuecos( rutaArchivo )

# tamanoHuecos = [1, 5, 1, 1, 1, 1, 1, 1, 1, 1, 8, 33, 1, 9, 2, 1, 1, 1, 1, 2, 30, 1, 78, 1, 1, 4, 3, 4, 1, 1, 1, 2, 1, 2, 6, 1, 2, 1, 1, 10, 1, 2, 1, 16, 1, 2, 1, 6, 17, 10, 1, 3, 2, 1, 13, 2, 1, 4, 1, 1, 4, 1, 4, 5, 3, 1, 1, 5, 1, 15, 20, 2, 9, 2, 2, 3, 45, 4, 2, 3, 12, 6, 1, 1, 4, 1, 1, 2, 4, 2, 1, 2, 1, 7, 3, 2, 1, 4, 1, 1]

# distanciaHuecos = [1.5701, 111.633, 141.142, 1.76438, 0.837771, 0.765392, 1.58342, 1.89765, 1.0791, 63.1789, 0.729455, 0.603634, 126.702, 109.495, 1436.22, 0.572933, 10.3598, 52.3773, 0.955911, 1.33628, 8.22581, 15.1502, 74.3603, 29.1455, 1.8667, 19.9296, 24.9794, 39.4969, 1946.04, 21.9534, 375.603, 2.15481, 1.5096, 217.552, 0.945876, 16.1518, 103.021, 40.3806, 0.46556, 45.2146, 39.9264, 2.64088, 0.392247, 59.244, 53.3621, 74.3719, 1.48546, 14.0504, 41.8667, 98.6225, 77.948, 1.02824, 21.6022, 65.7309, 4.83695, 2.1699, 1.3164, 179.241, 72.3316, 14.5574, 1.33267, 0.535268, 4.54222, 4.80008, 0.925769, 115.227, 215.23, 151.618, 27.2223, 1.1477, 4.81751, 1.04186, 5.56397, 2.2994, 66.7372, 257.649, 43.8446, 186.389, 1.16982, 250.105, 48.2281, 1.75601, 30.2361, 0.558533, 63.7874, 0.760415, 11.4586, 1.29953, 157.12, 11.4705, 1.65299, 21.2505, 0.266463, 1.53415, 68.3052, 1.29416, 0.866073, 1.31513, 1.69641, 152.332]

tamanoHuecos = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

distanciaHuecos = [1.5701, 111.633, 141.142, 1.76438, 0.837771, 0.765392, 1.58342, 1.89765, 1.0791, 63.1789, 0.729455, 0.603634, 126.702, 109.495, 1436.22, 0.572933, 10.3598, 52.3773, 0.955911, 1.33628]


print len(tamanoHuecos)

print len(distanciaHuecos)

for x in xrange(0, len(distanciaHuecos) ):
	distanciaHuecos[x] = int ( math.ceil(distanciaHuecos[x]) ) 

posicion = 0

archivoConHuecos = copy.copy(archivo)

trainingSet = int(( len(archivoConHuecos) * 0.80 ))

for y in xrange(0, len(distanciaHuecos) ):

	tamano = int (tamanoHuecos[y])
	posicion = posicion + int (distanciaHuecos[y]) 

	for z in xrange(0, tamano):

		if( posicion < trainingSet ):
			archivoConHuecos[posicion] = -1	
			posicion += 1

archivoHuecosCSV = "seriesTiempoConPocosHuecos/serieTiempoConPocosHuecos_"+ str(nb_archivo) +".csv"

# archivoHuecosCSV = "prueba_"+ str(nb_archivo) +".csv"

# Helper.guardarExcel(archivoHuecosCSV, archivo, archivoConHuecos, 'Original', 'Con Huecos')

with open(archivoHuecosCSV, "wb") as ArchivoNuevoCSV3:

    writer = csv.writer(ArchivoNuevoCSV3, lineterminator='\n')

    # writer.writerow(['Huecos', ''])

    for i in range(0, len(archivoConHuecos)):

        distanciaHuecosTemp2 = str(archivoConHuecos[i])

        aux1 = distanciaHuecosTemp2.replace("[", "")
        tarAdd = aux1.replace("]", "")

        writer.writerow( (distanciaHuecosTemp2, "") )