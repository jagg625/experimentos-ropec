# Obtener huecos

import Helper
import neurolab as nl
import numpy as np
import csv
from collections import Counter

directorio = 'datosExperimentosMuestras/'


datosArchivos = np.array([
                        ['wind_aristeomercado_10m_muestra'],
                        ['wind_cointzio_10m_muestra'],
                        ['wind_corrales_10m_muestra'],
                        ['wind_elfresno_10m_muestra']
                        # ['wind_lapalma_10m_muestra'],
                        # ['wind_lapiedad_10m_muestra'],
                        # ['wind_malpais_10m_muestra'],
                        # ['wind_markazuza_10m_muestra'],
                        # ['wind_melchorocampo_10m_muestra'],
                        # ['wind_patzcuaro_10m_muestra']
                    ])

for k in range (0, len(datosArchivos)):

    nb_archivo = datosArchivos[k][0]

    # nb_archivo = 'wind_lapalma_10m_complete'
    print "Archivo: ", nb_archivo

    rutaArchivo = directorio + nb_archivo+'.csv'

    archivo = Helper.funcionLeerArchivo2( rutaArchivo )

    arrayHuecos, arrayDistancia = Helper.getNumHuecos(archivo) 

    print "arrayDistancia: ", arrayDistancia
    print ""
    print "arrayHuecos: ", arrayHuecos
    print ""


    # print "tamano-arrayHuecos: ", len(arrayHuecos), ", tamano-arrayDistancia", len(arrayDistancia)

    # print "arrayHuecos: "
    # print arrayHuecos
    # print ""

    arrayObjectsHuecos = dict(Counter(arrayHuecos))

    print "arrayObjectsHuecos: "
    print arrayObjectsHuecos
    print ""

    idHuecos = arrayObjectsHuecos.keys()
    numHuecos = arrayObjectsHuecos.values()  

    print "idHuecos: "
    print idHuecos
    print ""

    print "numHuecos: "
    print numHuecos
    print ""

    print "tamano-idHuecos: ", len(idHuecos), ", tamano-numHuecos", len(numHuecos)

# archivoCSV = "huecos/" + str(nb_archivo) + "/archivoNumeroHuecos_"+ str(nb_archivo) +".csv"

# with open(archivoCSV, "wb") as ArchivoNuevoCSV:

#     writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

#     writer.writerow(['TAMANO HUECO', 'TOTAL'])

#     for i in range(0, len(idHuecos)):

#     	idHuecosTemp = str(idHuecos[i])
#         numHuecosTemp = str(numHuecos[i])

#         aux1 = idHuecosTemp.replace("[", "")
#         tarAdd = aux1.replace("]", "")

#         aux2 = numHuecosTemp.replace("[", "")
#         outAdd = aux2.replace("]", "")

#         writer.writerow( (tarAdd, outAdd) )


# archivoDisanciasCSV = "huecos/" + str(nb_archivo) + "/archivoDistanciaEntreHuecoss_"+ str(nb_archivo) +".csv"

# with open(archivoDisanciasCSV, "wb") as ArchivoNuevoCSV2:

#     writer = csv.writer(ArchivoNuevoCSV2, lineterminator='\n')

#     writer.writerow(['Distancia', ''])

#     for i in range(0, len(arrayDistancia)):

#         distanciaHuecosTemp = str(arrayDistancia[i])

#         aux1 = distanciaHuecosTemp.replace("[", "")
#         tarAdd = aux1.replace("]", "")

#         writer.writerow( (distanciaHuecosTemp, "") )

# archivoHuecosCSV = "huecos/" + str(nb_archivo) + "/archivoHuecos_"+ str(nb_archivo) +".csv"

# with open(archivoHuecosCSV, "wb") as ArchivoNuevoCSV3:

#     writer = csv.writer(ArchivoNuevoCSV3, lineterminator='\n')

#     writer.writerow(['Huecos', ''])

#     for i in range(0, len(arrayHuecos)):

#         distanciaHuecosTemp2 = str(arrayHuecos[i])

#         aux1 = distanciaHuecosTemp2.replace("[", "")
#         tarAdd = aux1.replace("]", "")

#         writer.writerow( (distanciaHuecosTemp2, "") )