import math
import operator
from sklearn import neighbors, datasets
import numpy as np

class Knn:

    def __init__(self, knowledgeSet, knowledgeSetLabels, kNeighbors):
        self.knowledgeSet = knowledgeSet
        self.knowledgeSetLabels = knowledgeSetLabels
        self.kNeighbors = kNeighbors

    def run( self, features):
        self.features = features

        '''
        kneighbors = self.getNeighbors()

        predict = self.getResponse( kneighbors )

        print "NO SKLEARN PREDICTION",predict

        '''

        clf = neighbors.KNeighborsRegressor(self.kNeighbors, weights='uniform')
        clf.fit(np.array(self.knowledgeSet), np.array(self.knowledgeSetLabels))

        targetFeaturesShaped = np.array(self.features).reshape(1, -1)

        prediction = clf.predict(targetFeaturesShaped)

        #print "SKLEARN PREDICTION", prediction

    	return prediction[0]

    def euclideanDistance(self, instance1, instance2):
    	distance = 0
        length = len( self.features )

    	for x in range( length ):
            distance += pow((instance1[x] - instance2[x]), 2) #formula de distancia euclidiana
    	return math.sqrt(distance)

    def getNeighbors( self ):

    	distances = []

    	#calcula la distancia euclidiana, la agrega a distances y las ordena
    	for knowledgeSetIndex in range( len ( self.knowledgeSet ) ):

            dist = self.euclideanDistance(self.features, self.knowledgeSet[ knowledgeSetIndex ])
            distances.append( ( self.knowledgeSetLabels[ knowledgeSetIndex ], dist ) )

    	distances.sort(key=operator.itemgetter(1))

    	neighbors = []
    	#extrar las n vecinas
    	for kNeighborsIndex in range( self.kNeighbors ):
            neighbors.append( distances[ kNeighborsIndex ] )

    	return neighbors

    def getResponse(self, neighbors):
        neighborsFinal = []

        for neighbors in neighbors:
            neighborsFinal.append(neighbors[0])

    	return self.listAverage( neighborsFinal )

    def getAccuracy(self, testY, predictions):
    	correct = 0
    	for x in range(len(testY)):
    		if testY[x] == predictions[x]:
    			correct += 1
    	return (correct/float(len(testX))) * 100.0

    def modaLista(self, lista):
        aux = 0
        cont = 0
        moda = -1
        lista.sort(cmp=None, key=None, reverse=False)
        for i in range(0,len(lista)-1):
            if (lista[i] == lista[i+1]):
                cont = cont + 1
                if cont >= aux:
                    aux = cont
                    moda = lista[i]
            else:
                cont=0

        return moda

    def listAverage(self, list):
        sum=0.0
        for i in range( 0, len( list ) ):
            sum = sum+list[i]
        return sum/len(list)
