import Helper
from Knn import Knn
import numpy as np
np.set_printoptions(threshold=np.inf)
from time import gmtime, strftime

def pronostico(datos, knowledgeSetPercent, hastaW = None, hastaK = None):

    print strftime("%Y-%m-%d %H:%M:%S", gmtime())

    lenKnowledgeSet = int(( len(datos) * knowledgeSetPercent ))

    knowledgeSet = archivo[:lenKnowledgeSet]
    validationSet = archivo[lenKnowledgeSet:]

    MSE = 999999999999999
    BESTFEATURESIZE = 0
    BESTKNEIGHBORS = 0
    BESTIMPVALIDATIONSETLABEL = []
    BESTPRONOSTICOS = []

    if hastaW != None:
        rangeFeatureSize = range(hastaW, hastaW + 1)
    else:
        rangeFeatureSize = range(2, 60)

    for featureSize in rangeFeatureSize:

        inpKnowledgeSet, tarKnowledgeSet = Helper.formatInputTarget(knowledgeSet, featureSize)

        inpValidationSet, tarValidationSet = Helper.formatInputTarget(validationSet, featureSize)

        inpKnowledgeSetLabels = []
        for tarKnowledge in tarKnowledgeSet:
            inpKnowledgeSetLabels.append(tarKnowledge[0])

        inpValidationSetLabels = []
        for tarValidation in tarValidationSet:
            inpValidationSetLabels.append(tarValidation[0])

        if hastaK != None:
            rangeK = range(hastaK, hastaK + 1)
        else:
            rangeK = range(2, 60)

        for k in rangeK:
            kNeighbors = k

            pronosticos = []
            for index, features in enumerate(inpValidationSet):

                knn = Knn(inpKnowledgeSet, inpKnowledgeSetLabels, kNeighbors)
                pronostico = knn.run(features)
                pronosticos.append(pronostico)

            MSEAux =  Helper.MSE(pronosticos, inpValidationSetLabels)

            print "featureSize :", featureSize, "kNeighbors : ", kNeighbors, " : MSE : ", MSEAux

            if MSEAux < MSE :
                print "<<<<<<< featureSize :", featureSize, "kNeighbors : ", kNeighbors, " : MSE : ", MSEAux, " >>>>>>"

                MSE = MSEAux
                FEATURESIZE = featureSize
                KNEIGHBORS  = kNeighbors
                BESTIMPVALIDATIONSETLABEL = inpValidationSetLabels
                BESTPRONOSTICOS = pronosticos

        print "AL MOMENTO: featureSize :", FEATURESIZE, "kNeighbors : ", KNEIGHBORS, " : MSE : ", MSE

    print "<<<<<<<<<<<<<<<< WINNER >>>>>>>>>>>>>>>"
    print "featureSize :", FEATURESIZE, "kNeighbors : ", KNEIGHBORS, " : MSE : ", MSE

    #print "BESTIMPVALIDATIONSETLABEL : ", np.reshape(BESTIMPVALIDATIONSETLABEL, (len(BESTIMPVALIDATIONSETLABEL), 1))
    #print "BESTPRONOSTICOS : ", np.reshape(BESTPRONOSTICOS, (len(BESTPRONOSTICOS), 1))

    tar = np.reshape(BESTIMPVALIDATIONSETLABEL, (len(BESTIMPVALIDATIONSETLABEL), 1))
    out = np.reshape(BESTPRONOSTICOS, (len(BESTPRONOSTICOS), 1))

    print strftime("%Y-%m-%d %H:%M:%S", gmtime())

    return FEATURESIZE, KNEIGHBORS, tar, out

    #Helper.Graficar(BESTIMPVALIDATIONSETLABEL, BESTPRONOSTICOS)


def reconstruct(originalSerie, knowledgeSetPercent, featureSize, kNeighbors):

    lenKnowledgeSet = int(( len(originalSerie) * knowledgeSetPercent ))

    knowledgeSet = originalSerie[:lenKnowledgeSet]

    inpKnowledgeSet, tarKnowledgeSet = Helper.formatInputTarget(knowledgeSet, featureSize)

    inpKnowledgeSetLabels = []
    for tarKnowledge in tarKnowledgeSet:
        inpKnowledgeSetLabels.append(tarKnowledge[0])

    reconstructedSeries = []
    for i in range(len(originalSerie)):
        original = originalSerie[i]

        if original == "NA" and len(reconstructedSeries) >= featureSize:

            features = reconstructedSeries[i - featureSize : i]

            if "NA" not in features:
                knn = Knn(inpKnowledgeSet, inpKnowledgeSetLabels, kNeighbors)
                pronostico = knn.run(features)

                reconstructedSeries.append(pronostico)
            else:
                reconstructedSeries.append(original)
        else:
            reconstructedSeries.append(original)

    #print "originalSerie : ", np.reshape(originalSerie, (len(originalSerie), 1))
    #print "reconstructed : ", np.reshape(reconstructedSeries, (len(reconstructedSeries), 1))

    tar = np.reshape(originalSerie, (len(originalSerie), 1))
    out = np.reshape(reconstructedSeries, (len(reconstructedSeries), 1))

    return tar, out

if __name__ == "__main__":


    serieTiempo = 'wind_corrales'

    serieTiempoReconstruida = True

    numeroMuestra = 3000

    # CON RECONSTRUCCION
    if serieTiempoReconstruida:
        rutaArchivo = 'resultadosSeriesTiempoConPocosHuecos/'+serieTiempo+'/'
        destino = 'resultadosSeriesTiempoConPocosHuecos/'+serieTiempo+'/'
        nombreArchivo = "serieTiempoConPocosHuecos_"+serieTiempo+"_10m_muestra_reconstruidoKNN"
        archivo = Helper.funcionLeerArchivo2( rutaArchivo + nombreArchivo + ".csv" )
    # SIN RECONSTRUCCION
    else:
        rutaArchivo = 'seriesTiempoConPocosHuecos/'
        destino = 'resultadosSeriesTiempoConPocosHuecos/'+serieTiempo+'/'
        nombreArchivo = "serieTiempoConPocosHuecos_"+serieTiempo+"_10m_muestra"
        archivo = Helper.obtenerArchivo( rutaArchivo + nombreArchivo + ".csv", numeroMuestra)
    
    # REALIZAR RECONSTRUCCION
    conRecontruccion = False

    print nombreArchivo +  ".csv"

    porcentajeTraningSet = 0.80
    nombreArchivoPronostico = destino + nombreArchivo + "_pronosticoKNN_CR.csv"
    w, k, tar, out = pronostico(archivo, porcentajeTraningSet)

    print "Archivo con pronostico KNN : ", nombreArchivoPronostico
    Helper.guardarExcel(nombreArchivoPronostico, tar, out, 'Target', 'Output')

    if  conRecontruccion :
        nombreArchivoRecontruido = destino + nombreArchivo + "_reconstruidoKNN.csv"
        tar, out = reconstruct(archivo, porcentajeTraningSet, w, k)

        print "Archivo con serie reconstruida : ", nombreArchivoRecontruido

        Helper.guardarExcel(nombreArchivoRecontruido, tar, out, 'Original', 'Reconstructed')