# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np

# Sin reconstruccion
directorio = 'seriesTiempoConPocosHuecos/'

# Con reconstruccion
#directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_aristeomercado/'
#directorio = 'datosExperimentosMuestras/'

# Con reconstruccion
#directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_aristeomercado/'
directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_cointzio/'

numeroMuestra =  3000

# [nombreArchivo, nE, nO, aE]
datosArchivos = np.array([
                        # ['serieTiempoConPocosHuecos_wind_aristeomercado_10m_muestra_reconstruidoANN', 49,2,7]#0.0086807474959
                        # ['serieTiempoConPocosHuecos_wind_cointzio_10m_muestra',22,18,6]
                        # ['serieTiempoConPocosHuecos_wind_aristeomercado_10m_muestra',3,7,7]# error: 0.00833914436287
                        # ['serieTiempoConPocosHuecos_wind_aristeomercado_10m_muestra_reconstruidoKNN', 30,7,7]#0.00826358069403
                        ['wind_cointzio_10m_muestra', 23, 5, 7]#0.0086807474959
                        #['serieTiempoConPocosHuecos_wind_cointzio_10m_muestra_reconstruidoANN',60,3,6]
                        #['serieTiempoConPocosHuecos_wind_cointzio_10m_muestra_reconstruidoKNN',61,4,6]
                    ])

for i in range (0, len(datosArchivos)):

    nb_archivo = datosArchivos[i][0]
    rutaArchivo = directorio + nb_archivo+'.csv'

    print "Generando la mejor Red para la serie : "+ rutaArchivo

    # Sin reconstruccion
    archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    # Reconstruccion
    #archivo = Helper.funcionLeerArchivo2( rutaArchivo )


    # print archivo

    numeroValidacion = int(( len(archivo) * 0.20 ))

    numeroTraining = numeroMuestra - numeroValidacion

    arrayError  = []
    arrayNet    = [] 

    for j in range(0,3):

        error, net = redNeuronal.run( int( datosArchivos[i][1] ) , int ( datosArchivos[i][2]), int(datosArchivos[i][3]), 6000, archivo, numeroTraining)

        print "Error ", (j+1)," : ", error

        arrayError.append(error)
        arrayNet.append(net)

    mejorError = min(arrayError)

    print "Mejor Error: ", mejorError

    posicionMejorNet = np.argmin(arrayError)
    mejorNet =  arrayNet[posicionMejorNet]

    mejorNet.save(('resultadosSeriesTiempoConPocosHuecos/wind_corrales/MejorN_'+ str(nb_archivo) +'.net'))

    # print "Red neuronal generada : " + nombreRedNeuronal

    # # Se simula el t + 1
    # ejecutaSimulacionRedNeuronalT1.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo)

    # # Se simula el t + 24
    # ejecutaSimulacionRedNeuronal.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo, numeroTraining)
