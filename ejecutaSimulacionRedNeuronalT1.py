# Experimentos de t+1
import simulacionRedNeuronalT1
import csv
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(threshold=np.inf)
import Helper
import math

print "T1"

# Sin reconstruccion
directorio = 'seriesTiempoConPocosHuecos/'
rutaSerieArchivo = 'resultadosSeriesTiempoConPocosHuecos/wind_corrales/'

# Con reconstruccion
# directorio = 'resultadosSeriesTiempoConPocosHuecos/wind_corrales/'

numeroMuestra =  3000

t = 1

datosArchivos = np.array([
                        # ['serieTiempoConPocosHuecos_wind_aristeomercado_10m_muestra',3,7,7]# error: 0.00833914436287
                        # ['serieTiempoConPocosHuecos_wind_aristeomercado_10m_muestra_reconstruidoKNN', 30,7,7],
                        # ['serieTiempoConPocosHuecos_wind_aristeomercado_10m_muestra_reconstruidoANN', ]
                        # ['serieTiempoConPocosHuecos_wind_cointzio_10m_muestra', 22,18, 6 ]
                        #['serieTiempoConPocosHuecos_wind_cointzio_10m_muestra_reconstruidoANN', 60,3, 6 ]
                        ['serieTiempoConPocosHuecos_wind_corrales_10m_muestra', 23, 5, 7]
                    ])

# Posicion del archivo a simular
x = 0

neuronasCentrada = int ( datosArchivos[x][1] )
neuronasCOculta = int  ( datosArchivos[x][2] )
algoritmoEntrenamiento = int ( datosArchivos[x][3] )

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv'

print "Generando pronostico para la serie : ", rutaArchivo

# Sin reconstruccion
#archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

# Reconstruccion
archivo = Helper.funcionLeerArchivo2( rutaArchivo )

numeroValidacion = int(( len(archivo) * 0.20 ))

numeroTraining = numeroMuestra - numeroValidacion

archivo = archivo[numeroTraining :]

out, tar = simulacionRedNeuronalT1.run( neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, nb_archivo, archivo )

# print "out : ", out
# print "tar : ", tar

nombreArchivoPronostico = rutaSerieArchivo + nb_archivo + "_pronosticoANN.csv"
print "Archivo con pronostico ANN : ", nombreArchivoPronostico
Helper.guardarExcel(nombreArchivoPronostico, tar, out, "Target", "Output")