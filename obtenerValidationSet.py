import Helper
import neurolab as nl
import numpy as np
import csv

directorio = 'seriesTiempoConHuecos/'

numeroMuestra =  2800

datosArchivos = np.array([
                        ['serieTiempoConHuecos_wind_cointzio_10m_muestra'],
                        ['serieTiempoConHuecos_wind_corrales_10m_muestra'],
                        ['serieTiempoConHuecos_wind_elfresno_10m_muestra'],
                        ['serieTiempoConHuecos_wind_malpais_10m_muestra'],
                        ['serieTiempoConHuecos_wind_lapalma_10m_muestra']
                    ])

# for i in range (0, len(datosArchivos)):

i = 4

nb_archivo = datosArchivos[i][0]
rutaArchivo = directorio + nb_archivo+'.csv'

archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

numeroValidacion = int(( len(archivo) * 0.20 ))

numeroTraining = numeroMuestra - numeroValidacion

validationSet = archivo[numeroTraining:]

# Crear archivo .csv
archivoCSV = "validationSet/"+str(nb_archivo)+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['ValidationSet', ''])

    for i in range(0, len(validationSet)):

    	tarTemp = str(validationSet[i])

        aux1 = tarTemp.replace("[", "")
        tarAdd = aux1.replace("]", "")

        writer.writerow( (tarAdd, '') )